package chevsavshev.com.cipher.util;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ByteHexConverterTest {
    private static final byte[] TEST_BYTES =
            {-1, 1, 2};

    private static final String TEST_HEX = "FF0102";

    private ByteHexConverter mByteHexConverter;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mByteHexConverter = new ByteHexConverter();
    }

    @Test
    public void testConvertByteToHex() {
        String hex = mByteHexConverter.bytesToHex(TEST_BYTES);

        assertThat(hex, is(TEST_HEX));
    }

    @Test
    public void testConvertHexToByte() {
        byte[] bytes = mByteHexConverter.hexToByte(TEST_HEX);

        assertThat(bytes, is(TEST_BYTES));
    }
}
