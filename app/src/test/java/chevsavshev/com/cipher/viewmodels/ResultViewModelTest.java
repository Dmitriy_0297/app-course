package chevsavshev.com.cipher.viewmodels;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.service.repository.clipboard.ClipboardRepository;
import chevsavshev.com.cipher.service.repository.localstorage.FileRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.viewmodel.ResultViewModel;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ResultViewModelTest {
    private static final String FILE_NAME = "kek";
    private static final String RESULT_TEXT = "lol";
    private static final String RESULT_KEY = "key";

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    private ResultViewModel viewModel;

    @Mock
    private Logger mLogger;
    @Mock
    private FileRepository mFileRepository;
    @Mock
    private ClipboardRepository mClipboardRepository;
    @Mock
    private CipherModel mEncryptionCipherModel;
    @Mock
    private CipherModel mDecryptionCipherModel;

    @Mock
    private Observer<Void> mEventObserver;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        when(mEncryptionCipherModel.getText()).thenReturn(RESULT_TEXT);
        when(mEncryptionCipherModel.getCipherKey()).thenReturn(RESULT_KEY);

        when(mDecryptionCipherModel.getText()).thenReturn(RESULT_TEXT);
        when(mDecryptionCipherModel.getCipherKey()).thenReturn(null);

        viewModel = new ResultViewModel(mLogger, mFileRepository, mClipboardRepository);
    }

    // TODO: add tests for saving text to file

    @Test
    public void testSetEncryptionCipherResult() {
        viewModel.setCipherResult(mEncryptionCipherModel);

        assertThat(viewModel.getResultText().get(), is(RESULT_TEXT));
        assertThat(viewModel.getHasKey().get(), is(true));
        assertThat(viewModel.getEncryptionKey().get(), is(RESULT_KEY));
    }

    @Test
    public void testSetDecryptionCipherResult() {
        viewModel.setCipherResult(mDecryptionCipherModel);

        assertThat(viewModel.getResultText().get(), is(RESULT_TEXT));
        assertThat(viewModel.getHasKey().get(), is(false));
    }

    @Test
    public void testCopyText() {
        viewModel.setCipherResult(mEncryptionCipherModel);
        viewModel.onCopyText();

        verify(mClipboardRepository, times(1))
                .copyToClipboard(RESULT_TEXT);
    }

    @Test
    public void testCopyKey() {
        viewModel.setCipherResult(mEncryptionCipherModel);
        viewModel.onCopyKey();

        verify(mClipboardRepository, times(1))
                .copyToClipboard(RESULT_KEY);
    }

    @Test
    public void testShareText() {
        viewModel.setCipherResult(mEncryptionCipherModel);
        viewModel.onShareText();

        assertEquals(RESULT_TEXT, viewModel.getShareTextEvent().getValue());
    }

    @Test
    public void testOnSaveText() {
        viewModel.getChooseFileEvent().observeForever(mEventObserver);
        viewModel.onSaveText();

        verify(mEventObserver).onChanged(nullable(Void.class));
    }

    @Test
    public void testOnFileNameProvided() {
        viewModel.setCipherResult(mEncryptionCipherModel);

        viewModel.onFileNameProvided(FILE_NAME);
        verify(mFileRepository).saveText(eq(FILE_NAME),eq(RESULT_TEXT));
    }
}
