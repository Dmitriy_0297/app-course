package chevsavshev.com.cipher.viewmodels;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.LiveData;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import chevsavshev.com.cipher.service.model.Cipher;
import chevsavshev.com.cipher.service.model.encryptor.StandardEncryptor;
import chevsavshev.com.cipher.service.repository.cipher.CiphersRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.viewmodel.EncryptionViewModel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

public class EncryptionViewModelTest {
    private static final String PLAIN_WORD = "lol";
    private static final String CIPHER_WORD = "eke";

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    private StandardEncryptor mEncryptor;
    @Mock
    private CiphersRepository mCiphersRepository;
    @Mock
    private Logger mLogger;
    @Mock
    private Cipher mCipher;

    @Mock
    private LiveData<List<Cipher>> mCipherList;

    private EncryptionViewModel mViewModel;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        doReturn(mCipherList).when(mCiphersRepository).getCipherList();

        mViewModel = new EncryptionViewModel(mLogger, mEncryptor, mCiphersRepository, PLAIN_WORD);
    }

    @Test
    public void testEncryption() {
        when(mEncryptor.cipherText(PLAIN_WORD)).thenReturn(CIPHER_WORD);

        mViewModel.encryptText(mCipher);

        assertEquals(CIPHER_WORD, mViewModel.getEncryptionResultObservable().getValue().getText());
    }

    @Test
    public void testCiphersList() {
        assertEquals(mCipherList, mViewModel.getCiphersListObservable());
    }
}
