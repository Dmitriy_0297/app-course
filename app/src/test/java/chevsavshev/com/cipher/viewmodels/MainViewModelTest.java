package chevsavshev.com.cipher.viewmodels;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;
import android.net.Uri;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import chevsavshev.com.cipher.service.repository.localstorage.StoreCipherRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.util.SharedPreferencesManager;
import chevsavshev.com.cipher.viewmodel.MainViewModel;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MainViewModelTest {
    private static final int MAX_CHARACTER_COUNT = 500000;

    private static final String PLAIN_TEXT = "lol";
    private static final String CIPHERED_TEXT = "kek";

    private MainViewModel mViewModel;

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    private Logger mLogger;

    @Mock
    private StoreCipherRepository mStoreCipherRepository;

    @Mock
    private SharedPreferencesManager mSharedPreferencesManager;

    @Mock
    private Uri mFileUri;

    @Mock
    private Observer<Void> mEventObserver;

    @Mock
    private Observer<String> mNavigationEventObserver;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        mViewModel = new MainViewModel(mLogger, mStoreCipherRepository, mSharedPreferencesManager, MAX_CHARACTER_COUNT);
    }

    @Test
    public void testInvalidEmptyText() {
        mViewModel.onInputChange("");
        assertFalse(mViewModel.getIsTextValid().get());
    }

    @Test
    public void testValidText() {
        mViewModel.onInputChange("bla bla bla");
        assertTrue(mViewModel.getIsTextValid().get());
    }

    @Test
    public void testTextValidityChange() {
        mViewModel.onInputChange("");
        assertFalse(mViewModel.getIsTextValid().get());

        mViewModel.onInputChange("asdfsa");
        assertTrue(mViewModel.getIsTextValid().get());

        mViewModel.onInputChange("");
        assertFalse(mViewModel.getIsTextValid().get());

        mViewModel.onInputChange("azz");
        assertTrue(mViewModel.getIsTextValid().get());
    }

    @Test
    public void testInputText() {
        String testString = "test";

        mViewModel.onInputChange(testString);
        assertEquals(mViewModel.getSourceText().get(), testString);
    }

    @Test
    public void testCharacterCount() {
        String testString = "test";
        int testCount = testString.length();

        mViewModel.onInputChange(testString);
        assertEquals(testCount, mViewModel.getCharacterCount().get());
    }

    @Test
    public void testSavingSourceText() {
        String testString = "lol";

        mViewModel.onInputChange(testString);
        verify(mSharedPreferencesManager, times(1))
                .storeString(anyString(), eq(testString));
    }

    @Test
    public void testLoadSourceText() {
        verify(mSharedPreferencesManager, times(1))
                .getString(anyString(), eq(""));
    }

    @Test
    public void testOnPermissionStatusChanged() {
        mViewModel.getRequestPermissionEvent().observeForever(mEventObserver);

        mViewModel.onPermissionStatusChanged(true);

        assertThat(mViewModel.isFilePermissionGranted(), is(true));
    }

    @Test
    public void testOnTextImport() {
        mViewModel.getChooseFileEvent().observeForever(mEventObserver);

        mViewModel.onPermissionStatusChanged(true);

        mViewModel.onChooseFile();

        verify(mEventObserver).onChanged(nullable(Void.class));
    }

    @Test
    public void testFileExtensionValidationInvalid() {
        when(mFileUri.getPath()).thenReturn("lol.kek");

        mViewModel.getInvalidFileExtensionEvent()
                .observeForever(mEventObserver);

        mViewModel.onFileChosen(mFileUri);

        verify(mEventObserver).onChanged(nullable(Void.class));
    }

    @Test
    public void testLoadFile() {
        final String TEST_TEXT = "lol";
        when(mStoreCipherRepository.loadText(mFileUri))
                .thenReturn(TEST_TEXT);

        when(mFileUri.getPath()).thenReturn("lol.txt");

        mViewModel.getInvalidFileExtensionEvent()
                .observeForever(mEventObserver);

        mViewModel.onFileChosen(mFileUri);

        verify(mEventObserver, never())
                .onChanged(nullable(Void.class));
        assertEquals(TEST_TEXT, mViewModel.getSourceText().get());
    }

    @Test
    public void testNavigateToEncryption() {
        mViewModel.getNavigateToEncryptionEvent().observeForever(mNavigationEventObserver);

        mViewModel.onInputChange(PLAIN_TEXT);
        mViewModel.onEncryptionChosen();

        verify(mNavigationEventObserver).onChanged(eq(PLAIN_TEXT));
    }

    @Test
    public void testNavigateToDecryption() {
        mViewModel.getNavigateToDecryptionEvent().observeForever(mNavigationEventObserver);

        mViewModel.onInputChange(PLAIN_TEXT);
        mViewModel.onDecryptionChosen();

        verify(mNavigationEventObserver).onChanged(eq(PLAIN_TEXT));
    }

    @Test
    public void testMaxCharacters() {
        assertThat(mViewModel.getMaxCharacters().get(), is(MAX_CHARACTER_COUNT));
    }
}
