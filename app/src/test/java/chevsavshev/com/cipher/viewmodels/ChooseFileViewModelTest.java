package chevsavshev.com.cipher.viewmodels;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import chevsavshev.com.cipher.viewmodel.ChooseFileViewModel;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class ChooseFileViewModelTest {
    private static final String VALID_FILE_NAME = "lol";
    private static final String INVALID_FILE_NAME = "";

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    private Observer<String> mEventObserver;

    private ChooseFileViewModel mChooseFileViewModel;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        mChooseFileViewModel = new ChooseFileViewModel();
    }

    @Test
    public void testInvalidText() {
        mChooseFileViewModel.onInputChange(INVALID_FILE_NAME);
        assertFalse(mChooseFileViewModel.getIsFileNameValid().get());
    }

    @Test
    public void testValidText() {
        mChooseFileViewModel.onInputChange(VALID_FILE_NAME);
        assertTrue(mChooseFileViewModel.getIsFileNameValid().get());
    }

    @Test
    public void testTextValidityChange() {
        mChooseFileViewModel.onInputChange(INVALID_FILE_NAME);
        assertFalse(mChooseFileViewModel.getIsFileNameValid().get());

        mChooseFileViewModel.onInputChange(VALID_FILE_NAME);
        assertTrue(mChooseFileViewModel.getIsFileNameValid().get());

        mChooseFileViewModel.onInputChange(INVALID_FILE_NAME);
        assertFalse(mChooseFileViewModel.getIsFileNameValid().get());

        mChooseFileViewModel.onInputChange(VALID_FILE_NAME);
        assertTrue(mChooseFileViewModel.getIsFileNameValid().get());
    }

    @Test
    public void testFileName() {
        mChooseFileViewModel.onInputChange(VALID_FILE_NAME);
        assertThat(mChooseFileViewModel.getFileName().get(), is(VALID_FILE_NAME));
    }

    @Test
    public void testFileNameProvidedEvent() {
        mChooseFileViewModel.getProvideFileNameEvent().observeForever(mEventObserver);
        mChooseFileViewModel.onInputChange(VALID_FILE_NAME);
        mChooseFileViewModel.onFileNameProvided();

        verify(mEventObserver).onChanged(eq(VALID_FILE_NAME));
    }
}
