package chevsavshev.com.cipher.viewmodels;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import chevsavshev.com.cipher.viewmodel.KeyViewModel;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

public class KeyViewModelTest {
    private static final String VALID_KEY = "123";
    private static final String INVALID_KEY = "";

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    private Observer<String> mEventObserver;

    private KeyViewModel mViewModel;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        mViewModel = new KeyViewModel();
    }

    @Test
    public void testInvalidText() {
        mViewModel.onInputChange(INVALID_KEY);
        assertFalse(mViewModel.getIsKeyValid().get());
    }

    @Test
    public void testValidText() {
        mViewModel.onInputChange(VALID_KEY);
        assertTrue(mViewModel.getIsKeyValid().get());
    }

    @Test
    public void testTextValidityChange() {
        mViewModel.onInputChange(INVALID_KEY);
        assertFalse(mViewModel.getIsKeyValid().get());

        mViewModel.onInputChange(VALID_KEY);
        assertTrue(mViewModel.getIsKeyValid().get());

        mViewModel.onInputChange(INVALID_KEY);
        assertFalse(mViewModel.getIsKeyValid().get());

        mViewModel.onInputChange(VALID_KEY);
        assertTrue(mViewModel.getIsKeyValid().get());
    }

    @Test
    public void testDecryptionKey() {
        mViewModel.onInputChange(VALID_KEY);
        assertThat(mViewModel.getDecryptionKey().get(), is(VALID_KEY));
    }

    @Test
    public void testOnKeyProvided() {
        mViewModel.getProvideKeyEvent().observeForever(mEventObserver);
        mViewModel.onInputChange(VALID_KEY);

        mViewModel.onKeyProvided();

        verify(mEventObserver).onChanged(eq(VALID_KEY));
    }
}
