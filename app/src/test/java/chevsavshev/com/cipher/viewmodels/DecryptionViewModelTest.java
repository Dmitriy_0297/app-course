package chevsavshev.com.cipher.viewmodels;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.security.InvalidKeyException;

import chevsavshev.com.cipher.service.model.Cipher;
import chevsavshev.com.cipher.service.model.encryptor.StandardEncryptor;
import chevsavshev.com.cipher.service.repository.cipher.CiphersRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.viewmodel.DecryptionViewModel;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DecryptionViewModelTest {
    private static final String PLAIN_WORD = "lol";
    private static final String CIPHER_WORD = "eke";
    private static final String CIPHER_NAME = "cipher";
    private static final String KEY = "key";

    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    private StandardEncryptor mEncryptor;
    @Mock
    private CiphersRepository mCiphers;
    @Mock
    private Logger mLogger;

    @Mock
    private Observer<Void> mEventObserver;

    @Mock
    private Cipher mCipher;

    private DecryptionViewModel mViewModel;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        mViewModel = new DecryptionViewModel(mLogger, mEncryptor, mCiphers, CIPHER_WORD);
    }

    @Test
    public void testDecryption() throws InvalidKeyException {
        when(mEncryptor.decipherText(eq(CIPHER_WORD), anyString())).thenReturn(PLAIN_WORD);

        mViewModel.onKeyProvided(KEY);

        assertEquals(PLAIN_WORD, mViewModel.getDecryptionResultObservable().getValue().getText());
    }

    @Test
    public void testOnCipherChosen() {
        doReturn(CIPHER_NAME).when(mCipher).getName();
        mViewModel.getRequestKeyEvent().observeForever(mEventObserver);

        mViewModel.onCipherChosen(mCipher);

        verify(mEventObserver).onChanged(nullable(Void.class));
        verify(mEncryptor).setAlgorithm(eq(CIPHER_NAME));
    }

    @Test
    public void testInvalidKeyException() throws InvalidKeyException {
        mViewModel.getInvalidKeyEvent().observeForever(mEventObserver);
        doThrow(InvalidKeyException.class).when(mEncryptor).decipherText(any(String.class), any(String.class));

        mViewModel.onKeyProvided(KEY);

        verify(mEventObserver).onChanged(nullable(Void.class));
    }
}
