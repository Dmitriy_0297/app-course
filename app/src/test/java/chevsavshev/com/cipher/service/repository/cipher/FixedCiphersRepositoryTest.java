package chevsavshev.com.cipher.service.repository.cipher;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import chevsavshev.com.cipher.service.model.Cipher;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

public class FixedCiphersRepositoryTest {
    @Rule
    public TestRule mTestRule = new InstantTaskExecutorRule();

    @Mock
    private Observer<List<Cipher>> mCipherListObserver;

    private FixedCiphersRepository mFixedCiphersRepository;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        mFixedCiphersRepository = new FixedCiphersRepository();
    }

    @Test
    public void testGetCiphers() {
        mFixedCiphersRepository.getCipherList().observeForever(mCipherListObserver);

        verify(mCipherListObserver).onChanged(any(List.class));
    }
}
