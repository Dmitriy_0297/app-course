package chevsavshev.com.cipher.service.model.encryptor;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.security.Security;

import chevsavshev.com.cipher.util.ByteHexConverter;
import chevsavshev.com.cipher.util.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class SecretKeyGeneratorTest {
    private static final String ALGORITHM_NAME = "DES";

//    private static final byte[] BYTE_KEY = new byte[]{};
    private static final String HEX_KEY = "kek";

    private SecretKeyGenerator mSecretKeyGenerator;

    @NonNull
    private ByteHexConverter mByteHexConverter;

    @Mock
    private Logger mLogger;

    @Before
    public void onStartUp() {
        MockitoAnnotations.initMocks(this);
        Security.addProvider(new org.spongycastle.jce.provider.BouncyCastleProvider());

        mSecretKeyGenerator = new SecretKeyGenerator(mByteHexConverter, mLogger);
    }

    @Test
    public void testGenerateKey() {
        mSecretKeyGenerator.generateKey(ALGORITHM_NAME);

        assertThat(mSecretKeyGenerator.getSecretKey(), is(notNullValue()));
    }

    //TODO: SC security exception

//    @Test
//    public void testEncodedKey() {
//        mSecretKeyGenerator.generateKey(ALGORITHM_NAME);
//
//        doReturn(HEX_KEY).when(mByteHexConverter).bytesToHex(any(byte[].class));
//
//        String encodedKey = mSecretKeyGenerator.getSecretKeyEncoded();
//        assertThat(encodedKey, is(HEX_KEY));
//    }
}
