package chevsavshev.com.cipher.service.repository.localstorage;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.FileNotFoundException;
import java.io.InputStream;

import chevsavshev.com.cipher.util.Logger;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class FileRepositoryTest {
    private static final String FILE_NAME = "lol";
    private static final String SAVED_TEXT = "kek";

    private FileRepository mFileRepository;

    @Mock
    private Logger mLogger;

    @Mock
    private ContentResolver mContentResolver;

    @Mock
    private InputStream mInputStream;

    @Mock
    private Context mContext;

    @Mock
    private Uri mUri;

    @Before
    public void onStartup() throws FileNotFoundException {
        MockitoAnnotations.initMocks(this);

        doReturn(mContentResolver).when(mContext).getContentResolver();
        doReturn(mInputStream).when(mContentResolver).openInputStream(any(Uri.class));

        mFileRepository = new FileRepository(mLogger, mContext);
    }

    @Test
    public void testSaveText() {
        mFileRepository.saveText(FILE_NAME, SAVED_TEXT);

        verify(mContext).getExternalFilesDir(null);
    }

    @Test
    public void testLoadText() throws FileNotFoundException {
        mFileRepository.loadText(mUri);

        verify(mContentResolver).openInputStream(mUri);
    }
}
