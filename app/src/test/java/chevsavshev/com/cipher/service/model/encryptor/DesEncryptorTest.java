package chevsavshev.com.cipher.service.model.encryptor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import chevsavshev.com.cipher.util.Logger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

public class DesEncryptorTest {
    private static final String PLAIN_TEXT = "lol";
    private static final String INVALID_CIPHERED_TEXT = "ZZ";
    private static final String VALID_CIPHERED_TEXT = "FF";
    private static final String VALID_KEY = "5A";

    private static final String ALGORITHM_NAME_DES = "DES";
    private static final String ALGORITHM_NAME_BLOWFISH = "BLOWFISH";

    private DesEncryptor mEncryptor;

    @Mock
    Logger mLogger;

    @Mock
    CipherProvider mCipherProvider;

    @Mock
    SecretKey mSecretKey;

    @Mock
    SecretKeyGenerator mSecretKeyGenerator;

    @Mock
    Cipher mCipher;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        doReturn(mSecretKey).when(mSecretKeyGenerator).getSecretKey();

        doReturn(VALID_CIPHERED_TEXT).when(mCipherProvider).encrypt(eq(PLAIN_TEXT), anyString(), any(SecretKey.class));
        doReturn(PLAIN_TEXT).when(mCipherProvider).decrypt(eq(VALID_CIPHERED_TEXT), anyString(), anyString(), anyString());

        doReturn(VALID_KEY).when(mSecretKeyGenerator).getSecretKeyEncoded();

        mEncryptor = new DesEncryptor(mLogger, ALGORITHM_NAME_DES, mCipherProvider, mSecretKeyGenerator);
    }

    @Test
    public void testEncryption() {
        mEncryptor.setAlgorithm(ALGORITHM_NAME_DES);

        String encryptedText = mEncryptor.cipherText(PLAIN_TEXT);

        assertThat(encryptedText, is(VALID_CIPHERED_TEXT));
    }

    @Test
    public void testValidDecryption() {
        mEncryptor.setAlgorithm(ALGORITHM_NAME_DES);

        String decryptedText = mEncryptor.decipherText(VALID_CIPHERED_TEXT, VALID_KEY);

        assertEquals(PLAIN_TEXT, decryptedText);
    }

    @Test
    public void testGetEncodedKey() {
        mEncryptor.setAlgorithm(ALGORITHM_NAME_DES);

        String encodedKey = mEncryptor.getKey();

        assertThat(encodedKey, is(VALID_KEY));
    }
}
