package chevsavshev.com.cipher.service.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CipherModel implements Parcelable {
    public static final Creator<CipherModel> CREATOR = new Creator<CipherModel>() {
        @Override
        public CipherModel createFromParcel(Parcel in) {
            return new CipherModel(in);
        }

        @Override
        public CipherModel[] newArray(int size) {
            return new CipherModel[size];
        }
    };

    private String mText;
    private String mCipherKey;

    public CipherModel(String text, String cipherKey) {
        mText = text;
        mCipherKey = cipherKey;
    }

    public CipherModel(Parcel in) {
        mText = in.readString();
        mCipherKey = in.readString();
    }

    public String getText() {
        return mText;
    }

    public String getCipherKey() {
        return mCipherKey;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mText);
        dest.writeString(mCipherKey);
    }
}
