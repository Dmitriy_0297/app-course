package chevsavshev.com.cipher.service.repository.localstorage;

import android.net.Uri;

public interface StoreCipherRepository {
    String loadText(Uri fileUri);

    void saveText(String fileName, String text);
}
