package chevsavshev.com.cipher.service.repository.clipboard;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;

import java.lang.ref.WeakReference;

public class ClipboardRepository {
    private static final String TAG = ClipboardRepository.class.getSimpleName();

    private WeakReference<Context> mContext;

    public ClipboardRepository(Context context) {
        mContext = new WeakReference<>(context);
    }

    public void copyToClipboard(String text) {
        Context context = mContext.get();

        if (context == null) {
            Log.e(TAG, "context is null");
            return;
        }

        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("label", text);
        clipboardManager.setPrimaryClip(clipData);
    }
}
