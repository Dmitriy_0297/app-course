package chevsavshev.com.cipher.service.model.encryptor;

import android.support.annotation.NonNull;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import chevsavshev.com.cipher.util.ByteHexConverter;
import chevsavshev.com.cipher.util.Logger;

public class SecretKeyGenerator {
    private static final String TAG = SecretKeyGenerator.class.getSimpleName();

    @NonNull
    private final ByteHexConverter mByteHexConverter;

    private SecretKey mSecretKey;
    private Logger mLogger;

    public SecretKeyGenerator(@NonNull ByteHexConverter byteHexConverter,
                              @NonNull Logger logger) {

        mByteHexConverter = byteHexConverter;
        mLogger = logger;
    }

    public void generateKey(String algorithmName) {
        try {
            mSecretKey = generateSecretKey(algorithmName);
        } catch (NoSuchProviderException | NoSuchAlgorithmException e) {
            mLogger.e(TAG, "lol: " + e.getMessage());
        }
    }

    public String getSecretKeyEncoded() {
        return mByteHexConverter.bytesToHex(mSecretKey.getEncoded());
    }

    public SecretKey getSecretKey() {
        return mSecretKey;
    }

    private SecretKey generateSecretKey(String algorithmName)
            throws NoSuchProviderException, NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithmName, "SC");

        return keyGenerator.generateKey();
    }


}
