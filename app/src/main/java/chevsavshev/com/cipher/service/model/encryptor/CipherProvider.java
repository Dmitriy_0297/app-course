package chevsavshev.com.cipher.service.model.encryptor;

import android.support.annotation.NonNull;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import chevsavshev.com.cipher.util.ByteHexConverter;

public class CipherProvider {
    private static final String CHAR_SET_NAME = "UTF-8";

    @NonNull
    private final ByteHexConverter mByteHexConverter;

    public CipherProvider(@NonNull ByteHexConverter byteHexConverter) {

        mByteHexConverter = byteHexConverter;
    }

    public String encrypt(String plainText, String fullAlgorithmName, SecretKey secretKey) {
        try {
            Cipher cipher = Cipher.getInstance(fullAlgorithmName, "SC");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] input = plainText.getBytes(CHAR_SET_NAME);

            byte[] encoded = cipher.doFinal(input);

            return mByteHexConverter.bytesToHex(encoded);
        } catch (NoSuchAlgorithmException | NoSuchProviderException
                | NoSuchPaddingException | InvalidKeyException
                | UnsupportedEncodingException | BadPaddingException
                | IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        return null;

    }

    public String decrypt(String cipherText, String algorithmName, String algorithmSettings, String key) {
        String algorithmNameWithSettings = algorithmName + algorithmSettings;

        try {
            Cipher cipher = Cipher.getInstance(algorithmNameWithSettings, "SC");
            byte[] encodedKey = mByteHexConverter.hexToByte(key);

            SecretKey secretKeySpec = new SecretKeySpec(encodedKey, algorithmName);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] utf8 = mByteHexConverter.hexToByte(cipherText);
            byte[] decodedText = cipher.doFinal(utf8);

            return new String(decodedText);
        } catch (NoSuchAlgorithmException | NoSuchProviderException
                | NoSuchPaddingException | InvalidKeyException
                | BadPaddingException
                | IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        return null;
    }
}
