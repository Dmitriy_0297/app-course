package chevsavshev.com.cipher.service.model;

public class Cipher {
    private long mId;
    private String mName;

    public Cipher(String name) {
        mName = name;
    }

    public long getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }
}
