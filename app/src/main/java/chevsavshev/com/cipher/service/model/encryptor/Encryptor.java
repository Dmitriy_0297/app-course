package chevsavshev.com.cipher.service.model.encryptor;

import java.security.InvalidKeyException;

public interface Encryptor {
    String cipherText(String plainText);

    String decipherText(String cipherText, String key) throws InvalidKeyException;

    String getKey();
}