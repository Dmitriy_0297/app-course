package chevsavshev.com.cipher.service.repository.cipher;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.Arrays;
import java.util.List;

import chevsavshev.com.cipher.service.model.Cipher;

public class FixedCiphersRepository implements CiphersRepository {
    private static final List<Cipher> CIPHERS_LIST = Arrays.asList(
            new Cipher("DES"),
            new Cipher("BLOWFISH"),
            new Cipher("AES"),
            new Cipher("RC6"),
            new Cipher("DESede"),
            new Cipher("RC2"),
            new Cipher("IDEA")
    );

    private MutableLiveData<List<Cipher>> mCipherListObservable = new MutableLiveData<>();

    public FixedCiphersRepository() {
        mCipherListObservable.postValue(getCiphers());
    }

    @Override
    public LiveData<List<Cipher>> getCipherList() {
        return mCipherListObservable;
    }

    private List<Cipher> getCiphers() {
        return CIPHERS_LIST;
    }
}
