package chevsavshev.com.cipher.service.model.encryptor;

public interface StandardEncryptor extends Encryptor {
    void setAlgorithm(String algorithmName);
}
