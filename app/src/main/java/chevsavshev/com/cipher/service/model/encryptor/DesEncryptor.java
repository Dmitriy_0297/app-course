package chevsavshev.com.cipher.service.model.encryptor;

import android.support.annotation.NonNull;

import java.security.Security;

import chevsavshev.com.cipher.util.Logger;

public class DesEncryptor implements StandardEncryptor {
    private static final String TAG = DesEncryptor.class.getSimpleName();

    private static final String ALGORITHM_SETTINGS = "/ECB/PKCS5Padding";

    static {
        Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);
        Security.removeProvider("BC");
    }

    private Logger mLogger;
    private CipherProvider mCipherProvider;
    private SecretKeyGenerator mSecretKeyGenerator;

    private String mAlgorithmName;
    private String mAlgorithmNameWithSettings;

    public DesEncryptor(Logger logger, String algorithmName, CipherProvider cipherProvider, SecretKeyGenerator secretKeyGenerator) {
        super();

        mLogger = logger;
        mCipherProvider = cipherProvider;
        mSecretKeyGenerator = secretKeyGenerator;

        setAlgorithm(algorithmName);

        generateKey();
    }

    @Override
    public void setAlgorithm(String algorithmName) {
        mAlgorithmName = algorithmName;
        mAlgorithmNameWithSettings = mAlgorithmName + ALGORITHM_SETTINGS;

        generateKey();
    }

    @Override
    public String cipherText(@NonNull String plainText) {
        return mCipherProvider.encrypt(plainText, mAlgorithmNameWithSettings, mSecretKeyGenerator.getSecretKey());
    }

    @Override
    public String decipherText(String cipherText, String key) {
        return mCipherProvider.decrypt(cipherText, mAlgorithmName, ALGORITHM_SETTINGS, key);
    }

    @Override
    public String getKey() {
        return mSecretKeyGenerator.getSecretKeyEncoded();
    }

    private void generateKey() {
        mSecretKeyGenerator.generateKey(mAlgorithmName);
    }
}