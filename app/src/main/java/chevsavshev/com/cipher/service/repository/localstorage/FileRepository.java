package chevsavshev.com.cipher.service.repository.localstorage;

import android.content.Context;
import android.net.Uri;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;

import chevsavshev.com.cipher.util.Logger;

public class FileRepository implements StoreCipherRepository {
    private static final String TAG = FileRepository.class.getSimpleName();

    private static final String PLAIN_TEXT_EXTENSION = ".txt";

    private Logger mLogger;

    private WeakReference<Context> mContext;

    public FileRepository(Logger logger, Context context) {
        mLogger = logger;
        mContext = new WeakReference<>(context);
    }

    @Override
    public String loadText(Uri uri) {
        Context context = mContext.get();

        String result = null;
        if (context != null) {
            InputStream inputStream;
            try {
                inputStream = context.getContentResolver().openInputStream(uri);
                result = readTextFromStream(inputStream);
            } catch (IOException e) {
                mLogger.e(TAG, "IOException: " + e.getMessage());
            }
        }

        return result;
    }

    @Override
    public void saveText(String fileName, String text) {
        mLogger.d(TAG, "saveText");
        Context context = mContext.get();

        if (context == null) {
            mLogger.e(TAG, "saveText: context is null");
            return;
        }

        if(!fileName.endsWith(PLAIN_TEXT_EXTENSION)) {
            fileName += PLAIN_TEXT_EXTENSION;
        }

        File file = new File(context.getExternalFilesDir(null), fileName);

        try (
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
        ) {
            outputStreamWriter.write(text);
        } catch (IOException e) {
            mLogger.e(TAG, "Saving text failed: " + e.getMessage());
        }
    }

    private String readTextFromStream(InputStream inputStream) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(inputStreamReader);
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
        }

        return stringBuilder.toString();
    }
}
