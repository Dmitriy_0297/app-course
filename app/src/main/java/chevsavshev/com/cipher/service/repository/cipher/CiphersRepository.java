package chevsavshev.com.cipher.service.repository.cipher;


import android.arch.lifecycle.LiveData;

import java.util.List;

import chevsavshev.com.cipher.service.model.Cipher;

public interface CiphersRepository {
    LiveData<List<Cipher>> getCipherList();
}
