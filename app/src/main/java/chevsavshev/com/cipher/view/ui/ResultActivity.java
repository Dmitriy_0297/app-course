package chevsavshev.com.cipher.view.ui;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import chevsavshev.com.cipher.R;
import chevsavshev.com.cipher.databinding.ActivityResultBinding;
import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.service.repository.clipboard.ClipboardRepository;
import chevsavshev.com.cipher.service.repository.localstorage.FileRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.view.callback.ChooseFileNameCallback;
import chevsavshev.com.cipher.viewmodel.ResultViewModel;

public class ResultActivity extends AppCompatActivity {
    private static final String TAG = ResultActivity.class.getSimpleName();

    private static final String DIALOG_TAG = "choose-file-dialog";

    private final Logger mLogger = new Logger();
    private ResultViewModel mResultViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        initDataBinding();

        CipherModel result = getIntent().getParcelableExtra(Intent.EXTRA_TEXT);
        mResultViewModel.setCipherResult(result);

        mResultViewModel.getShareTextEvent().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String textToShare) {
                shareText(textToShare);
            }
        });

        mResultViewModel.getChooseFileEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                showChooseFileDialog();
            }
        });
    }

    private void showChooseFileDialog() {
        ChooseFileDialog dialog = new ChooseFileDialog();
        dialog.setChooseFileNameCallback(new ChooseFileNameCallback() {
            @Override
            public void onFileNameProvided(String fileName) {
                mResultViewModel.onFileNameProvided(fileName);
            }
        });
        dialog.show(getSupportFragmentManager(), DIALOG_TAG);
    }

    private void initDataBinding() {
        ActivityResultBinding resultBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_result);

        Logger logger = new Logger();
        FileRepository fileRepository = new FileRepository(logger, getApplicationContext());
        ClipboardRepository clipboardRepository = new ClipboardRepository(getApplicationContext());
        mResultViewModel = new ResultViewModel(mLogger, fileRepository, clipboardRepository);

        resultBinding.setViewModel(mResultViewModel);
    }

    private void shareText(String text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.setType("text/plain");
        startActivity(shareIntent);
    }
}
