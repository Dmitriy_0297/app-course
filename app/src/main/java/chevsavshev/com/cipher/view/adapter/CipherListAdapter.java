package chevsavshev.com.cipher.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;
import java.util.Objects;

import chevsavshev.com.cipher.R;
import chevsavshev.com.cipher.databinding.CipherListItemBinding;
import chevsavshev.com.cipher.service.model.Cipher;
import chevsavshev.com.cipher.view.callback.CipherClickCallback;

public class CipherListAdapter extends RecyclerView.Adapter<CipherListAdapter.CipherViewHolder> {
    @Nullable
    private final CipherClickCallback mCipherClickCallback;
    private List<? extends Cipher> mCipherList;

    public CipherListAdapter(@Nullable CipherClickCallback clickCallback) {
        mCipherClickCallback = clickCallback;
    }

    public void setCipherList(final List<? extends Cipher> cipherList) {
        if (mCipherList == null) {
            mCipherList = cipherList;
            notifyItemRangeInserted(0, cipherList.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mCipherList.size();
                }

                @Override
                public int getNewListSize() {
                    return cipherList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mCipherList.get(oldItemPosition).getId() == cipherList.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Cipher newCipher = cipherList.get(newItemPosition);
                    Cipher oldCipher = cipherList.get(oldItemPosition);

                    return newCipher.getId() == oldCipher.getId() && Objects.equals(newCipher.getName(),
                            oldCipher.getName());
                }
            });

            mCipherList = cipherList;

            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull CipherListAdapter.CipherViewHolder holder, int position) {
        holder.mBinding.setCipher(mCipherList.get(position));
        holder.mBinding.executePendingBindings();
    }

    @NonNull
    @Override
    public CipherListAdapter.CipherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CipherListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.cipher_list_item, parent, false);

        binding.setCallback(mCipherClickCallback);

        return new CipherViewHolder(binding);
    }

    @Override
    public int getItemCount() {
        return mCipherList == null ? 0 : mCipherList.size();
    }

    static class CipherViewHolder extends RecyclerView.ViewHolder {
        final CipherListItemBinding mBinding;

        CipherViewHolder(CipherListItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

    }
}
