package chevsavshev.com.cipher.view.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;

import chevsavshev.com.cipher.R;
import chevsavshev.com.cipher.databinding.DialogDecryptionKeyBinding;
import chevsavshev.com.cipher.view.callback.KeyProvidedCallback;
import chevsavshev.com.cipher.viewmodel.KeyViewModel;

public class DecryptionKeyDialog extends AppCompatDialogFragment {
    private KeyViewModel mKeyViewModel;

    private KeyProvidedCallback mKeyProvidedCallback;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mKeyViewModel = new KeyViewModel();
        Activity activity = getActivity();
        DialogDecryptionKeyBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_decryption_key, null, false);
        binding.setViewModel(mKeyViewModel);

        return new AlertDialog.Builder(activity, AlertDialog.BUTTON_NEUTRAL)
                .setView(binding.getRoot())
                .create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mKeyViewModel.getProvideKeyEvent().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String decryptionKey) {
                if(mKeyProvidedCallback != null) {
                    mKeyProvidedCallback.onKeyProvided(decryptionKey);
                }
            }
        });
    }

    public void setOnKeyProvidedCallback(KeyProvidedCallback keyProvidedCallback) {
        mKeyProvidedCallback = keyProvidedCallback;
    }
}
