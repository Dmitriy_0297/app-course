package chevsavshev.com.cipher.view.ui;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import chevsavshev.com.cipher.R;
import chevsavshev.com.cipher.databinding.ActivityMainBinding;
import chevsavshev.com.cipher.service.repository.localstorage.FileRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.util.SharedPreferencesManager;
import chevsavshev.com.cipher.viewmodel.MainViewModel;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    public static final int MAX_CHARACTER_COUNT = 500000;

    private final static int READ_PERMISSION_REQUEST_CODE = 1;
    private final static int READ_REQUEST_CODE = 2;

    private final static String PERMISSION_NAME = Manifest.permission.READ_EXTERNAL_STORAGE;

    private final Logger mLogger = new Logger();
    private MainViewModel mMainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDataBinding();

        mMainViewModel.onPermissionStatusChanged(hasPermission(PERMISSION_NAME));

        mMainViewModel.getNavigateToEncryptionEvent().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String plainText) {
                Intent intent = new Intent(MainActivity.this, EncryptionActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, plainText);
                startActivity(intent);
            }
        });

        mMainViewModel.getNavigateToDecryptionEvent().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String plainText) {
                Intent intent = new Intent(MainActivity.this, DecryptionActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, plainText);
                startActivity(intent);
            }
        });

        mMainViewModel.getChooseFileEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                performFileSearch();
            }
        });

        mMainViewModel.getRequestPermissionEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{PERMISSION_NAME},
                        READ_PERMISSION_REQUEST_CODE);
            }
        });

        mMainViewModel.getInvalidFileExtensionEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                String errorMessage = getResources().getString(R.string.invalid_file_extension_error_message);
                Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mLogger.d(TAG, "onActivtyResult");

        switch(requestCode) {
            case READ_REQUEST_CODE:
                if(resultCode == RESULT_OK) {
                    mMainViewModel.onFileChosen(data.getData());
                }

                break;
            case READ_PERMISSION_REQUEST_CODE:
                if(resultCode == RESULT_OK) {
                    mLogger.d(TAG, "read permission request");
                    mMainViewModel.onPermissionStatusChanged(true);
                }

                break;
        }
    }

    private void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/plain");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    private boolean hasPermission(String permissionName) {
        int permission = ActivityCompat.checkSelfPermission(this, permissionName);
        return permission == PackageManager.PERMISSION_GRANTED;
    }

    private void initDataBinding() {
        ActivityMainBinding activityMainBinding = DataBindingUtil.
                setContentView(this, R.layout.activity_main);

        Logger logger = new Logger();
        FileRepository fileRepository = new FileRepository(logger, getApplicationContext());
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(getApplicationContext());
        mMainViewModel = new MainViewModel(mLogger, fileRepository, sharedPreferencesManager, MAX_CHARACTER_COUNT);
        activityMainBinding.setViewModel(mMainViewModel);
    }
}
