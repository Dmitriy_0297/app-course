package chevsavshev.com.cipher.view.callback;

import chevsavshev.com.cipher.service.model.Cipher;

public interface CipherClickCallback {
    void onClick(Cipher cipher);
}
