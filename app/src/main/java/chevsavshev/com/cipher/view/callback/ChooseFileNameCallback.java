package chevsavshev.com.cipher.view.callback;

public interface ChooseFileNameCallback {
    void onFileNameProvided(String fileName);
}
