package chevsavshev.com.cipher.view.ui;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import java.util.List;

import chevsavshev.com.cipher.R;
import chevsavshev.com.cipher.databinding.ActivityEncryptionBinding;
import chevsavshev.com.cipher.service.model.Cipher;
import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.service.model.encryptor.CipherProvider;
import chevsavshev.com.cipher.service.model.encryptor.DesEncryptor;
import chevsavshev.com.cipher.service.model.encryptor.SecretKeyGenerator;
import chevsavshev.com.cipher.service.repository.cipher.FixedCiphersRepository;
import chevsavshev.com.cipher.util.ByteHexConverter;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.view.adapter.CipherListAdapter;
import chevsavshev.com.cipher.view.callback.CipherClickCallback;
import chevsavshev.com.cipher.viewmodel.EncryptionViewModel;

public class EncryptionActivity extends AppCompatActivity {
    private static final String TAG = EncryptionActivity.class.getSimpleName();

    private final Logger mLogger = new Logger();

    CipherListAdapter mCipherListAdapter;
    private EncryptionViewModel mEncryptionViewModel;

    private final CipherClickCallback mCipherClickCallback = new CipherClickCallback() {
        @Override
        public void onClick(Cipher cipher) {
            mEncryptionViewModel.encryptText(cipher);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encryption);

        injectViewModelDependencies();

        initDataBinding();

        mEncryptionViewModel.getEncryptionResultObservable().observe(this, new Observer<CipherModel>() {
            @Override
            public void onChanged(@Nullable CipherModel encryptionResult) {
                openResultActivity(encryptionResult);
            }
        });

        mEncryptionViewModel.getCiphersListObservable().observe(this, new android.arch.lifecycle.Observer<List<Cipher>>() {
            @Override
            public void onChanged(@Nullable List<Cipher> ciphers) {
                mCipherListAdapter.setCipherList(ciphers);
            }
        });
    }

    private void openResultActivity(CipherModel encryptionResult) {
        Intent intent = new Intent(this,
                ResultActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, encryptionResult);
        startActivity(intent);
    }

    private void injectViewModelDependencies() {
        ByteHexConverter byteHexConverter = new ByteHexConverter();
        CipherProvider cipherProvider = new CipherProvider(byteHexConverter);
        SecretKeyGenerator secretKeyGenerator = new SecretKeyGenerator(byteHexConverter, mLogger);
        DesEncryptor encryptor = new DesEncryptor(mLogger, "DES", cipherProvider, secretKeyGenerator);
        FixedCiphersRepository ciphersRepository = new FixedCiphersRepository();
        String plainText = getIntent().getStringExtra(Intent.EXTRA_TEXT);
        mEncryptionViewModel = new EncryptionViewModel(mLogger, encryptor, ciphersRepository, plainText);
    }

    private void initDataBinding() {
        ActivityEncryptionBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_encryption);
        binding.setViewModel(mEncryptionViewModel);

        mCipherListAdapter = new CipherListAdapter(mCipherClickCallback);
        binding.encryptionCipherList.setLayoutManager(new LinearLayoutManager(this));
        binding.encryptionCipherList.setAdapter(mCipherListAdapter);
    }
}
