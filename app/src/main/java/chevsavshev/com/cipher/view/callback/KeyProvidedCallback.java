package chevsavshev.com.cipher.view.callback;

public interface KeyProvidedCallback {
    void onKeyProvided(String decryptionKey);
}
