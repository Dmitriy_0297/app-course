package chevsavshev.com.cipher.view.ui;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import java.util.List;

import chevsavshev.com.cipher.R;
import chevsavshev.com.cipher.databinding.ActivityDecryptionBinding;
import chevsavshev.com.cipher.service.model.Cipher;
import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.service.model.encryptor.CipherProvider;
import chevsavshev.com.cipher.service.model.encryptor.DesEncryptor;
import chevsavshev.com.cipher.service.model.encryptor.SecretKeyGenerator;
import chevsavshev.com.cipher.service.repository.cipher.FixedCiphersRepository;
import chevsavshev.com.cipher.util.ByteHexConverter;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.view.adapter.CipherListAdapter;
import chevsavshev.com.cipher.view.callback.CipherClickCallback;
import chevsavshev.com.cipher.view.callback.KeyProvidedCallback;
import chevsavshev.com.cipher.viewmodel.DecryptionViewModel;

public class DecryptionActivity extends AppCompatActivity {
    private static final String TAG = DecryptionActivity.class.getSimpleName();
    private static final String DIALOG_TAG = "provide key dialog";

    private final Logger mLogger = new Logger();
    private CipherListAdapter mCipherListAdapter;

    private DecryptionViewModel mDecryptionViewModel;
    private final CipherClickCallback mCipherClickCallback = new CipherClickCallback() {
        @Override
        public void onClick(Cipher cipher) {
            mDecryptionViewModel.onCipherChosen(cipher);
        }
    };
    private KeyProvidedCallback mKeyProvidedCallback = new KeyProvidedCallback() {
        @Override
        public void onKeyProvided(String decryptionKey) {
            mDecryptionViewModel.onKeyProvided(decryptionKey);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decryption);

        injectViewModelDependencies();

        initDataBinding();

        mDecryptionViewModel.getCiphersObservable().observe(this, new Observer<List<Cipher>>() {
            @Override
            public void onChanged(@Nullable List<Cipher> ciphers) {
                mCipherListAdapter.setCipherList(ciphers);
            }
        });

        mDecryptionViewModel.getRequestKeyEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                showKeyDialog();
            }
        });

        mDecryptionViewModel.getDecryptionResultObservable().observe(this, new Observer<CipherModel>() {
            @Override
            public void onChanged(@Nullable CipherModel decryptionResult) {
                openResultActivity(decryptionResult);
            }
        });

        mDecryptionViewModel.getInvalidKeyEvent().observe(this, new Observer<Void>() {
            @Override
            public void onChanged(@Nullable Void aVoid) {
                String errorMessage = getResources().getString(R.string.invalid_key_message);
                Toast.makeText(DecryptionActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                showKeyDialog();
            }
        });
    }

    private void injectViewModelDependencies() {
        ByteHexConverter byteHexConverter = new ByteHexConverter();
        CipherProvider cipherProvider = new CipherProvider(byteHexConverter);
        SecretKeyGenerator secretKeyGenerator = new SecretKeyGenerator(byteHexConverter, mLogger);
        DesEncryptor encryptor = new DesEncryptor(mLogger,"DES", cipherProvider, secretKeyGenerator);
        FixedCiphersRepository ciphersRepository = new FixedCiphersRepository();
        String encryptedText = getIntent().getStringExtra(Intent.EXTRA_TEXT);
        mDecryptionViewModel = new DecryptionViewModel(mLogger, encryptor, ciphersRepository, encryptedText);

    }

    private void initDataBinding() {
        ActivityDecryptionBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_decryption);
        binding.setViewModel(mDecryptionViewModel);

        mCipherListAdapter = new CipherListAdapter(mCipherClickCallback);
        binding.decryptionCipherList.setLayoutManager(new LinearLayoutManager(this));
        binding.decryptionCipherList.setAdapter(mCipherListAdapter);
    }

    private void openResultActivity(CipherModel decryptionResult) {
        Intent intent = new Intent(this,
                ResultActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, decryptionResult);
        startActivity(intent);
    }

    private void showKeyDialog() {
        DecryptionKeyDialog dialog = new DecryptionKeyDialog();
        dialog.setOnKeyProvidedCallback(mKeyProvidedCallback);
        dialog.show(getSupportFragmentManager(), DIALOG_TAG);
    }
}
