package chevsavshev.com.cipher.view.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;

import chevsavshev.com.cipher.R;
import chevsavshev.com.cipher.databinding.DialogChooseFileBinding;
import chevsavshev.com.cipher.view.callback.ChooseFileNameCallback;
import chevsavshev.com.cipher.viewmodel.ChooseFileViewModel;

public class ChooseFileDialog extends AppCompatDialogFragment {
    private static final String TAG = ChooseFileDialog.class.getName();

    private ChooseFileViewModel mChooseFileViewModel;

    private ChooseFileNameCallback mChooseFileNameCallback;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mChooseFileViewModel = new ChooseFileViewModel();

        Activity activity = getActivity();
        DialogChooseFileBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                R.layout.dialog_choose_file, null, false);
        binding.setViewModel(mChooseFileViewModel);

        return new AlertDialog.Builder(activity,
                AlertDialog.BUTTON_NEUTRAL)
                .setView(binding.getRoot())
                .create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mChooseFileViewModel.getProvideFileNameEvent().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String fileName) {
             if(mChooseFileNameCallback != null) {
                 mChooseFileNameCallback.onFileNameProvided(fileName);
                 dismiss();
             }
            }
        });
    }

    void setChooseFileNameCallback(ChooseFileNameCallback chooseFileNameCallback) {
        mChooseFileNameCallback = chooseFileNameCallback;
    }
}
