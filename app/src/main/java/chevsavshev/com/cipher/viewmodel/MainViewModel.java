package chevsavshev.com.cipher.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.net.Uri;
import android.support.annotation.NonNull;

import chevsavshev.com.cipher.service.repository.localstorage.StoreCipherRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.util.SharedPreferencesManager;
import chevsavshev.com.cipher.util.SingleLiveEvent;

public class MainViewModel extends ViewModel {
    public static final String SOURCE_TEXT_PREFERENCE_KEY = "source-text-preference";
    private static final String TAG = MainViewModel.class.getSimpleName();
    private static final String TEXT_FILE_EXTENSION = ".txt";

    private final ObservableField<String> mSourceText = new ObservableField<>();
    private final ObservableBoolean mIsTextValid = new ObservableBoolean();
    private final ObservableInt mCharacterCount = new ObservableInt();
    private final ObservableInt mMaxCharacters = new ObservableInt();

    private boolean mIsFilePermissionGranted;
    private Logger mLogger;
    private StoreCipherRepository mStoreRepository;
    private SharedPreferencesManager mSharedPreferencesManager;


    private SingleLiveEvent<String> mNavigateToEncryptionEvent = new SingleLiveEvent<>();
    private SingleLiveEvent<String> mNavigateToDecryptionEvent = new SingleLiveEvent<>();

    private SingleLiveEvent<Void> mCheckStoragePermissionEvent = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> mChooseFileEvent = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> mRequestPermissionEvent = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> mInvalidFileExtensionEvent = new SingleLiveEvent<>();

    public MainViewModel(Logger logger, StoreCipherRepository storeRepository,
                         SharedPreferencesManager sharedPreferencesManager,
                         int maxCharacterCount) {
        mLogger = logger;
        mStoreRepository = storeRepository;
        mSharedPreferencesManager = sharedPreferencesManager;

        mCharacterCount.set(0);
        mMaxCharacters.set(maxCharacterCount);

        loadSavedSourceText();
    }

    public SingleLiveEvent<String> getNavigateToEncryptionEvent() {
        return mNavigateToEncryptionEvent;
    }

    public SingleLiveEvent<String> getNavigateToDecryptionEvent() {
        return mNavigateToDecryptionEvent;
    }

    public SingleLiveEvent<Void> getChooseFileEvent() {
        return mChooseFileEvent;
    }

    public SingleLiveEvent<Void> getRequestPermissionEvent() {
        return mRequestPermissionEvent;
    }

    public SingleLiveEvent<Void> getInvalidFileExtensionEvent() {
        return mInvalidFileExtensionEvent;
    }

    public ObservableField<String> getSourceText() {
        return mSourceText;
    }

    public ObservableBoolean getIsTextValid() {
        return mIsTextValid;
    }

    public ObservableInt getCharacterCount() {
        return mCharacterCount;
    }

    public ObservableInt getMaxCharacters() {
        return mMaxCharacters;
    }

    public boolean isFilePermissionGranted() {
        return mIsFilePermissionGranted;
    }

    public void onInputChange(@NonNull String inputText) {
        mSourceText.set(inputText);
        validateInputText(inputText);

        mCharacterCount.set(inputText.length());

        mSharedPreferencesManager.storeString(SOURCE_TEXT_PREFERENCE_KEY, inputText);
    }

    public void onEncryptionChosen() {
        mNavigateToEncryptionEvent.setValue(mSourceText.get());
    }

    public void onDecryptionChosen() {
        mNavigateToDecryptionEvent.setValue(mSourceText.get());
    }

    private void onTextImport() {
        mLogger.d(TAG, "onTextImport");
        mChooseFileEvent.call();
    }

    public void onChooseFile() {
        if (mIsFilePermissionGranted) {
            onTextImport();
        } else {
            mRequestPermissionEvent.call();
        }
    }

    public void onPermissionStatusChanged(boolean isPermissionGranted) {
        mIsFilePermissionGranted = isPermissionGranted;
    }

    public void onFileChosen(@NonNull Uri fileUri) {
        mLogger.d(TAG, "onFileChosen: " + fileUri.getPath());
        if (validateFileExtension(fileUri)) {
            String sourceText = mStoreRepository.loadText(fileUri);
            onInputChange(sourceText);
        } else {
            mInvalidFileExtensionEvent.call();
        }
    }

    public void loadSavedSourceText() {
        String savedSourceText = mSharedPreferencesManager.getString(SOURCE_TEXT_PREFERENCE_KEY, "");
        mSourceText.set(savedSourceText);
    }

    private boolean validateFileExtension(Uri fileUri) {
        String path = fileUri.getPath();

        if (path == null) {
            mLogger.e(TAG, "validateFileExtension: path is null");
            return false;
        }

        String extension = path.substring(path.lastIndexOf("."));
        mLogger.d(TAG, "extension: " + extension);

        return extension.equals(TEXT_FILE_EXTENSION);
    }

    private void validateInputText(String text) {
        Boolean isValid = text != null
                && !text.isEmpty() && text.length() <= mMaxCharacters.get();
        mIsTextValid.set(isValid);
    }
}
