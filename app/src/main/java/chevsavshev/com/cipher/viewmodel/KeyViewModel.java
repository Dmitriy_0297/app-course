package chevsavshev.com.cipher.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import chevsavshev.com.cipher.util.SingleLiveEvent;

public class KeyViewModel extends ViewModel {
    private final ObservableBoolean mIsKeyValid = new ObservableBoolean();
    private final ObservableField<String> mDecryptionKey = new ObservableField<>();

    public SingleLiveEvent<String> getProvideKeyEvent() {
        return mProvideKeyEvent;
    }

    private SingleLiveEvent<String> mProvideKeyEvent = new SingleLiveEvent<>();

    public ObservableBoolean getIsKeyValid() {
        return mIsKeyValid;
    }

    public ObservableField<String> getDecryptionKey() {
        return mDecryptionKey;
    }

    public void onKeyProvided() {
        mProvideKeyEvent.setValue(mDecryptionKey.get());
    }

    public void onInputChange(String keyText) {
        mDecryptionKey.set(keyText);
        validateKeyText(keyText);
    }

    private void validateKeyText(String keyText) {
        boolean isValid = !keyText.isEmpty();
        mIsKeyValid.set(isValid);
    }
}
