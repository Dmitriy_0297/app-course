package chevsavshev.com.cipher.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import java.util.List;

import chevsavshev.com.cipher.service.model.Cipher;
import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.service.model.encryptor.StandardEncryptor;
import chevsavshev.com.cipher.service.repository.cipher.CiphersRepository;
import chevsavshev.com.cipher.util.Logger;

public class EncryptionViewModel extends ViewModel {

    @NonNull
    private Logger mLogger;
    @NonNull
    private StandardEncryptor mEncryptor;
    @NonNull
    private CiphersRepository mCiphersRepository;
    @NonNull
    private String mPlainText;

    private LiveData<List<Cipher>> mCiphersObservable;
    private MutableLiveData<CipherModel> mEncryptionResultObservable = new MutableLiveData<>();

    public EncryptionViewModel(@NonNull Logger logger, @NonNull StandardEncryptor encryptor,
                               @NonNull CiphersRepository ciphersRepository, @NonNull String plainText) {
        mLogger = logger;
        mEncryptor = encryptor;
        mCiphersRepository = ciphersRepository;
        mPlainText = plainText;

        mCiphersObservable = mCiphersRepository.getCipherList();
    }

    public LiveData<CipherModel> getEncryptionResultObservable() {
        return mEncryptionResultObservable;
    }

    public LiveData<List<Cipher>> getCiphersListObservable() {
        return mCiphersObservable;
    }

    public void encryptText(Cipher cipher) {
        mEncryptor.setAlgorithm(cipher.getName());
        String encryptedText = mEncryptor.cipherText(mPlainText);
        String encryptionKey = mEncryptor.getKey();

        CipherModel encryptionResult = new CipherModel(encryptedText, encryptionKey);
        mEncryptionResultObservable.postValue(encryptionResult);
    }
}
