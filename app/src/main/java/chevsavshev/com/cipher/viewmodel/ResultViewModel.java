package chevsavshev.com.cipher.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.service.repository.clipboard.ClipboardRepository;
import chevsavshev.com.cipher.service.repository.localstorage.FileRepository;
import chevsavshev.com.cipher.service.repository.localstorage.StoreCipherRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.util.SingleLiveEvent;

public class ResultViewModel extends ViewModel {
    private static final String TAG = ResultViewModel.class.getSimpleName();

    private final ObservableField<String> mResultText = new ObservableField<>();
    private final ObservableField<String> mEncryptionKey = new ObservableField<>();

    // TODO: It is a bad practice when your logic depends on some value being null or not null
    private final ObservableField<Boolean> mHasKey = new ObservableField<>();

    private SingleLiveEvent<String> mShareTextEvent = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> mChooseFileEvent = new SingleLiveEvent<>();

    private Logger mLogger;
    private StoreCipherRepository mFileRepository;
    private ClipboardRepository mClipboardRepository;

    public ResultViewModel(Logger logger, FileRepository fileRepository,
                           ClipboardRepository clipboardRepository) {
        mLogger = logger;
        mFileRepository = fileRepository;
        mClipboardRepository = clipboardRepository;
    }

    public ObservableField<String> getResultText() {
        return mResultText;
    }

    public ObservableField<String> getEncryptionKey() {
        return mEncryptionKey;
    }

    public ObservableField<Boolean> getHasKey() {
        return mHasKey;
    }

    public SingleLiveEvent<String> getShareTextEvent() {
        return mShareTextEvent;
    }

    public SingleLiveEvent<Void> getChooseFileEvent() {
        return mChooseFileEvent;
    }

    public void setCipherResult(CipherModel cipherResult) {
        mResultText.set(cipherResult.getText());

        String encryptionKey = cipherResult.getCipherKey();
        if (encryptionKey != null) {
            mHasKey.set(true);
            mEncryptionKey.set(encryptionKey);
        } else {
            mHasKey.set(false);
        }
    }

    //TODO: refactor ResultViewModel so it doesn't use values of observables inside the viewModel

    public void onSaveText() {
        mChooseFileEvent.call();
    }

    public void onCopyText() {
        copyToClipboard(mResultText.get());
    }

    public void onCopyKey() {
        copyToClipboard(mEncryptionKey.get());
    }

    public void onShareText() {
        mShareTextEvent.setValue(mResultText.get());
    }

    private void copyToClipboard(String text) {
        mClipboardRepository.copyToClipboard(text);
    }

    public void onFileNameProvided(String fileName) {
        mLogger.d(TAG, "file name provided: " + fileName);
        mFileRepository.saveText(fileName, mResultText.get());
    }
}
