package chevsavshev.com.cipher.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import chevsavshev.com.cipher.util.SingleLiveEvent;

public class ChooseFileViewModel extends ViewModel {
    private final ObservableBoolean mIsFileNameValid = new ObservableBoolean();
    private final ObservableField<String> mFileName = new ObservableField<>();

    private SingleLiveEvent<String> mProvideFileNameEvent = new SingleLiveEvent<>();

    public ObservableBoolean getIsFileNameValid() {
        return mIsFileNameValid;
    }

    public ObservableField<String> getFileName() {
        return mFileName;
    }

    public SingleLiveEvent<String> getProvideFileNameEvent() {
        return mProvideFileNameEvent;
    }

    public void onFileNameProvided() {
        mProvideFileNameEvent.setValue(mFileName.get());
    }

    public void onInputChange(String fileName) {
        mFileName.set(fileName);
        validateKeyText(fileName);
    }

    private void validateKeyText(String fileName) {
        boolean isValid = !fileName.isEmpty();
        mIsFileNameValid.set(isValid);
    }
}
