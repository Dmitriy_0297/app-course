package chevsavshev.com.cipher.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import java.security.InvalidKeyException;
import java.util.List;

import chevsavshev.com.cipher.service.model.Cipher;
import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.service.model.encryptor.StandardEncryptor;
import chevsavshev.com.cipher.service.repository.cipher.CiphersRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.util.SingleLiveEvent;

public class DecryptionViewModel extends ViewModel {
    private static final String TAG = DecryptionViewModel.class.getSimpleName();

    @NonNull
    private Logger mLogger;
    @NonNull
    private StandardEncryptor mEncryptor;
    @NonNull
    private CiphersRepository mCiphersRepository;

    private SingleLiveEvent<Void> mRequestKeyEvent = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> mInvalidKeyEvent = new SingleLiveEvent<>();
    private LiveData<List<Cipher>> mCiphersObservable;
    private MutableLiveData<CipherModel> mDecryptionResultObservable = new MutableLiveData<>();

    @NonNull
    private String mEncryptedText;

    public DecryptionViewModel(@NonNull Logger logger, @NonNull StandardEncryptor encryptor,
                               @NonNull CiphersRepository ciphersRepository, @NonNull String encryptedText) {
        mLogger = logger;
        mEncryptor = encryptor;
        mCiphersRepository = ciphersRepository;
        mEncryptedText = encryptedText;

        mCiphersObservable = mCiphersRepository.getCipherList();
    }

    public SingleLiveEvent<Void> getRequestKeyEvent() {
        return mRequestKeyEvent;
    }

    public SingleLiveEvent<Void> getInvalidKeyEvent() {
        return mInvalidKeyEvent;
    }

    public LiveData<List<Cipher>> getCiphersObservable() {
        return mCiphersObservable;
    }

    public LiveData<CipherModel> getDecryptionResultObservable() {
        return mDecryptionResultObservable;
    }

    public void onKeyProvided(String decryptionKey) {
        mLogger.d(TAG, "onKeyProvided: " + decryptionKey);
        String decryptedText;
        try {
            decryptedText = mEncryptor.decipherText(mEncryptedText, decryptionKey);
        } catch (InvalidKeyException e) {
            mLogger.e(TAG, "invalid key: " + e.getMessage());
            mInvalidKeyEvent.call();

            return;
        }

        CipherModel decryptionResult = new CipherModel(decryptedText, null);
        mDecryptionResultObservable.postValue(decryptionResult);
    }

    public void onCipherChosen(Cipher cipher) {
        mRequestKeyEvent.call();
        mEncryptor.setAlgorithm(cipher.getName());
    }
}
