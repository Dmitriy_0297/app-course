package chevsavshev.com.cipher;

import android.app.Instrumentation;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.view.ui.ResultActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class ResultActivityTest {
    private static final String RESULT_TEXT = "kek kek kek";
    private static final String CIPHER_KEY = "keyek";

    @Rule
    public ActivityTestRule<ResultActivity> activityRule = new ActivityTestRule<>(
            ResultActivity.class, true, false);

    @Before
    public void setUp() {
        Intents.init();
        Intent intent = new Intent();
        CipherModel cipherModel = new CipherModel(RESULT_TEXT, CIPHER_KEY);
        intent.putExtra(Intent.EXTRA_TEXT, cipherModel);

        activityRule.launchActivity(intent);
    }

//    RESULT_SCREEN_01
    @Test
    public void testGotEncryptedText() {
        onView(withId(R.id.result_text_view)).check(matches(withText(RESULT_TEXT)));
    }

//    RESULT_SCREEN_02
    @Test
    public void testCopyToClipboard() {
        onView(withId(R.id.copy_button)).perform(click());

        verifyTextIsInClipboard(RESULT_TEXT);

    }

    //    RESULT_SCREEN_03
    @Test
    public void testShareIntent() {
        intending(hasAction(Intent.ACTION_SEND))
                .respondWith(new Instrumentation.ActivityResult(0, null));

        onView(withId(R.id.share_button)).perform(click());

        intended(hasAction(Intent.ACTION_SEND));
    }

    @Test
    public void testShareIntentData() {
        intending(hasAction(Intent.ACTION_SEND))
                .respondWith(new Instrumentation.ActivityResult(0, null));

        onView(withId(R.id.share_button)).perform(click());

        intended(hasExtra(Intent.EXTRA_TEXT, RESULT_TEXT));
    }

//    DECRYPTION_CIPHER_CHOICE_04
    @Test
    public void testShowingChooseFileDialog() {
        onView(withId(R.id.save_button))
                .perform(click());

        onView(withId(R.id.choose_file_name_edit_text))
                .check(matches(isDisplayed()));
    }

//    ENCRYPTION_RESULT_SCREEN_01
    @Test
    public void testEncryptionKey() {
        onView(withId(R.id.result_key_text)).check(matches(withText(CIPHER_KEY)));
    }

//    ENCRYPTION_RESULT_SCREEN_02
    @Test
    public void testCopyKeyToClipboard() {
        onView(withId(R.id.copy_key_button)).perform(click());

        verifyTextIsInClipboard(CIPHER_KEY);
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    private void verifyTextIsInClipboard(final String text) {
        InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                ClipboardManager clipboardManager = (ClipboardManager) activityRule.getActivity()
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                assertThat(clipboardManager.getText().toString(), is(text));
            }
        });
    }
}
