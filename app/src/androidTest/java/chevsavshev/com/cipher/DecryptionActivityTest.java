package chevsavshev.com.cipher;

import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import chevsavshev.com.cipher.view.ui.DecryptionActivity;
import chevsavshev.com.cipher.view.ui.ResultActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static chevsavshev.com.cipher.common.Matchers.atPosition;
import static org.hamcrest.core.IsNot.not;

public class DecryptionActivityTest {
    private static final String TEST_CIPHER_NAME = "DES";
    private static final String TEST_ENCRYPTED_TEXT = TEST_CIPHER_NAME;
    private static final String VALID_KEY = "FF";

    @Rule
    public ActivityTestRule<DecryptionActivity> activityRule = new ActivityTestRule<>(
            DecryptionActivity.class, true, false);

    @Before
    public void setUp() {
        Intents.init();
        Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_TEXT, TEST_ENCRYPTED_TEXT);

        activityRule.launchActivity(intent);
    }

    @Test
    public void testPlainTextIntent() {
        intended(hasExtra(Intent.EXTRA_TEXT, TEST_ENCRYPTED_TEXT));
    }

    @Test
    public void testHasDes() {
        onView(withId(R.id.decryption_cipher_list))
                .check(matches(atPosition(0, hasDescendant(withText(TEST_CIPHER_NAME)))));
    }

//    DECRYPTION_CIPHER_CHOICE_01
    @Test
    public void testShowingKeyDialog() {
        onView(withId(R.id.decryption_cipher_list))
                .perform(RecyclerViewActions.actionOnItem(
                        hasDescendant(withText(TEST_CIPHER_NAME)),
                        click()));

        onView(withId(R.id.dialog_key_edit_text))
                .check(matches(isDisplayed()));
    }

//  DECRYPTION_CIPHER_CHOICE_03
    @Test
    public void testClosingKeyDialog() {
        onView(withId(R.id.decryption_cipher_list))
                .perform(RecyclerViewActions.actionOnItem(
                        hasDescendant(withText(TEST_CIPHER_NAME)),
                        click()));
        pressBack();

        intended(not(hasComponent(ResultActivity.class.getName())));
    }

    //  DECRYPTION_CIPHER_CHOICE_04
    @Test
    public void testKeyDialogInvalidKey() {
        onView(withId(R.id.decryption_cipher_list))
                .perform(RecyclerViewActions.actionOnItem(
                        hasDescendant(withText(TEST_CIPHER_NAME)),
                        click()));

        onView(withId(R.id.dialog_decipher_button))
                .check(matches(not(isEnabled())));

        intended(not(hasComponent(ResultActivity.class.getName())));
    }

    //  DECRYPTION_CIPHER_CHOICE_05
    @Test
    public void testKeyDialogValidKey() {
        onView(withId(R.id.decryption_cipher_list))
                .perform(RecyclerViewActions.actionOnItem(
                        hasDescendant(withText(TEST_CIPHER_NAME)),
                        click()));

        onView(withId(R.id.dialog_key_edit_text))
                .perform(typeText(VALID_KEY));

        onView(withId(R.id.dialog_decipher_button))
                .perform(click());

        intended(hasComponent(ResultActivity.class.getName()));
    }

    @After
    public void tearDown() {
        Intents.release();
    }
}
