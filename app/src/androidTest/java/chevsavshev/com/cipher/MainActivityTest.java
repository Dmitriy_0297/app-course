package chevsavshev.com.cipher;

import android.Manifest;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import chevsavshev.com.cipher.common.ViewActions;
import chevsavshev.com.cipher.view.ui.DecryptionActivity;
import chevsavshev.com.cipher.view.ui.EncryptionActivity;
import chevsavshev.com.cipher.view.ui.MainActivity;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    private static final int INVALID_CHARACTER_COUNT = MainActivity.MAX_CHARACTER_COUNT + 1;

    private static final String TEST_TEXT = "bla bla bla";
    private static final String INVALID_FILE_URI = "lol.zip";

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class, true, false);

    @Rule
    public GrantPermissionRule mGrantPermissionRule = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE);

    @Before
    public void setUp() {
        Intents.init();
        mActivityTestRule.launchActivity(null);
    }

    // MAIN_SCREEN_04
    // MAIN_SCREEN_05
    @Test
    public void testChooseButtonsDisabled() {
        Espresso.onView(withId(R.id.cipher_button))
                .check(matches(not(isEnabled())));
        Espresso.onView(withId(R.id.decipher_button))
                .check(matches(not(isEnabled())));
    }

    // MAIN_SCREEN_01
    @Test
    public void testChooseButtonsEnabled() {
        Espresso.onView(withId(R.id.source_text_view))
                .perform(typeText(TEST_TEXT), closeSoftKeyboard());

        Espresso.onView(withId(R.id.cipher_button))
                .check(matches(isEnabled()));
        Espresso.onView(withId(R.id.decipher_button))
                .check(matches(isEnabled()));
    }

    @Test
    public void testOutgoingIntentEncrypting() {
        Espresso.onView(withId(R.id.source_text_view))
                .perform(typeText(TEST_TEXT), closeSoftKeyboard());

        Espresso.onView(withId(R.id.cipher_button))
                .perform(click());
        intended(hasExtra(Intent.EXTRA_TEXT, TEST_TEXT));
    }

    @Test
    public void testOutgoingIntentDecrypting() {
        Espresso.onView(withId(R.id.source_text_view))
                .perform(typeText(TEST_TEXT), closeSoftKeyboard());

        Espresso.onView(withId(R.id.decipher_button))
                .perform(click());
        intended(hasExtra(Intent.EXTRA_TEXT, TEST_TEXT));
    }

    // MAIN_SCREEN_08
    @Test
    public void testNextActivityTransitionEncrypting() {
        Espresso.onView(withId(R.id.source_text_view))
                .perform(typeText(TEST_TEXT), closeSoftKeyboard());

        Espresso.onView(withId(R.id.cipher_button))
                .perform(click());
        intended(hasComponent(EncryptionActivity.class.getName()));
    }

    // MAIN_SCREEN_09
    @Test
    public void testNextActivityTransitionDecrypting() {
        Espresso.onView(withId(R.id.source_text_view))
                .perform(typeText(TEST_TEXT), closeSoftKeyboard());

        Espresso.onView(withId(R.id.decipher_button))
                .perform(click());
        intended(hasComponent(DecryptionActivity.class.getName()));
    }

    // MAIN_SCREEN_02
    @Test
    public void testImportButton() {
        intending(hasAction(Intent.ACTION_GET_CONTENT))
                .respondWith(new Instrumentation.ActivityResult(0, null));

        onView(withId(R.id.import_button)).perform(click());

        intended(hasAction(Intent.ACTION_GET_CONTENT));
    }

    // MAIN_SCREEN_03
    @Test
    public void testInvalidFileExtensionError() {
        Uri uri = Uri.parse(INVALID_FILE_URI);
        Intent resultData = new Intent(Intent.ACTION_GET_CONTENT, uri);
        intending(hasAction(Intent.ACTION_GET_CONTENT))
                .respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, resultData));

        onView(withId(R.id.import_button)).perform(click());

        onView(withText(R.string.invalid_file_extension_error_message))
                .inRoot(withDecorView(not(is(mActivityTestRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }

    // MAIN_SCREEN_06
    // MAIN_SCREEN_07
    @Test
    public void testTextLargerThanMaxWordCount() {
        char [] repeatedCharacter = new char[INVALID_CHARACTER_COUNT];
        Arrays.fill(repeatedCharacter, 'c');
        String text = new String(repeatedCharacter);

        onView(withId(R.id.source_text_view)).perform(ViewActions.writeTextInstantly(text), closeSoftKeyboard());

        Espresso.onView(withId(R.id.cipher_button))
                .check(matches(not(isEnabled())));
        Espresso.onView(withId(R.id.decipher_button))
                .check(matches(not(isEnabled())));
    }

    // MAIN_SCREEN_10
    @Test
    public void testSavingTextBetweenLaunches() {
        onView(withId(R.id.source_text_view)).perform(typeText(TEST_TEXT));
        mActivityTestRule.finishActivity();

        mActivityTestRule.launchActivity(null);
        onView(withId(R.id.source_text_view)).check(matches(withText(TEST_TEXT)));
    }

    @After
    public void tearDown() {
        Intents.release();

        //TODO: replace this workaround with something that does not clear real shared preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getInstrumentation().getTargetContext());
        preferences.edit().clear().commit();
    }
}
