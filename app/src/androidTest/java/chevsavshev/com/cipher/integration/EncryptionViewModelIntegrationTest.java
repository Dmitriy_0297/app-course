package chevsavshev.com.cipher.integration;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import chevsavshev.com.cipher.service.model.Cipher;
import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.service.model.encryptor.CipherProvider;
import chevsavshev.com.cipher.service.model.encryptor.DesEncryptor;
import chevsavshev.com.cipher.service.model.encryptor.SecretKeyGenerator;
import chevsavshev.com.cipher.service.model.encryptor.StandardEncryptor;
import chevsavshev.com.cipher.service.repository.cipher.CiphersRepository;
import chevsavshev.com.cipher.service.repository.cipher.FixedCiphersRepository;
import chevsavshev.com.cipher.util.ByteHexConverter;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.viewmodel.EncryptionViewModel;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(AndroidJUnit4.class)
public class EncryptionViewModelIntegrationTest {
    private static final String PLAIN_WORD = "lol";
    private static final String ENCRYPTED_WORD = "61057FFA437F9A52";
    private static final String CIPHER_NAME = "cipher";
    private static final String KEY = "297585752913D007";

    private static final String ALGORITHM_NAME = "DES";

    SecretKey mSecretKey;

    @Mock
    SecretKeyGenerator mSecretKeyGenerator;

    private StandardEncryptor mEncryptor;

    private CiphersRepository mCiphersRepository;
    @Mock
    private Logger mLogger;

    private EncryptionViewModel mViewModel;

    @Mock
    private Observer<CipherModel> mTest;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        ByteHexConverter byteHexConverter = new ByteHexConverter();
        CipherProvider cipherProvider = new CipherProvider(byteHexConverter);
        mEncryptor = new DesEncryptor(mLogger, ALGORITHM_NAME, cipherProvider, mSecretKeyGenerator);
        mCiphersRepository = new FixedCiphersRepository();

        mViewModel = new EncryptionViewModel(mLogger, mEncryptor, mCiphersRepository, PLAIN_WORD);

        byte[] keyBytes = byteHexConverter.hexToByte(KEY);
        mSecretKey = new SecretKeySpec(keyBytes, 0, keyBytes.length, ALGORITHM_NAME);
        doReturn(mSecretKey).when(mSecretKeyGenerator).getSecretKey();
    }

    @Test
    public void testEncrypt() {
        mViewModel.getEncryptionResultObservable().observeForever(mTest);
        Cipher cipher = new Cipher(ALGORITHM_NAME);
        Observer<CipherModel> cipherModelObserver = new Observer<CipherModel>() {
            @Override
            public void onChanged(@Nullable CipherModel cipherModel) {
                assertThat(cipherModel.getText(), is(ENCRYPTED_WORD));
            }
        };
        mViewModel.getEncryptionResultObservable().observeForever(cipherModelObserver);

        mViewModel.encryptText(cipher);
    }


}
