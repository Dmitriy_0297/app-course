package chevsavshev.com.cipher.integration;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import chevsavshev.com.cipher.service.repository.localstorage.StoreCipherRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.util.SharedPreferencesManager;
import chevsavshev.com.cipher.viewmodel.MainViewModel;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class MainViewModelIntegrationTest {
    private static final int MAX_CHARACTER_COUNT = 500000;

    private static final String TEST_TEXT = "lol";

    private MainViewModel mMainViewModel;

    private SharedPreferencesManager mSharedPreferencesManager;

    @Mock
    private Logger mLogger;

    @Mock
    private StoreCipherRepository mStoreCipherRepository;

    @Before
    public void onSetup() {
        MockitoAnnotations.initMocks(this);

        Context context = InstrumentationRegistry.getTargetContext();
        mSharedPreferencesManager = new SharedPreferencesManager(context);
        mMainViewModel = new MainViewModel(mLogger, mStoreCipherRepository, mSharedPreferencesManager, MAX_CHARACTER_COUNT);
    }

    @Test
    public void testLoadSharedPreferences() {
        SharedPreferences.Editor preferencesEditor = PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry.getTargetContext()).edit();
        preferencesEditor.putString(MainViewModel.SOURCE_TEXT_PREFERENCE_KEY, TEST_TEXT);
        preferencesEditor.commit();

        mMainViewModel.loadSavedSourceText();

        assertThat(mMainViewModel.getSourceText().get(), is(TEST_TEXT));
    }

    @Test
    public void testLoadSharedPreferencesDefault() {
        mMainViewModel.loadSavedSourceText();

        assertThat(mMainViewModel.getSourceText().get(), is(""));
    }

    @Test
    public void testSaveToSharedPreferences() {
        mMainViewModel.onInputChange(TEST_TEXT);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry.getTargetContext());
        String savedText = sharedPreferences.getString(MainViewModel.SOURCE_TEXT_PREFERENCE_KEY, null);

        assertThat(savedText, is(TEST_TEXT));
    }

    @After
    public void onTearDown() {
        SharedPreferences.Editor preferencesEditor = PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry.getTargetContext()).edit();
        preferencesEditor.clear();
        preferencesEditor.commit();
    }
}
