package chevsavshev.com.cipher.integration;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import chevsavshev.com.cipher.service.model.CipherModel;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class CipherModelTest {
    private static final String ENCRYPTED_TEXT = "lol";
    private static final String ENCRYPTION_KEY = "kek";

    @Test
    public void testCipherModel() {
        CipherModel cipherModel = new CipherModel(ENCRYPTED_TEXT, ENCRYPTION_KEY);

        Parcel cipherParcel = Parcel.obtain();
        cipherModel.writeToParcel(cipherParcel,cipherModel.describeContents());
        cipherParcel.setDataPosition(0);

        CipherModel cipherModelFromParcel = CipherModel.CREATOR.createFromParcel(cipherParcel);

        assertThat(cipherModelFromParcel.getText(), is(ENCRYPTED_TEXT));
        assertThat(cipherModelFromParcel.getCipherKey(), is(ENCRYPTION_KEY));
    }
}
