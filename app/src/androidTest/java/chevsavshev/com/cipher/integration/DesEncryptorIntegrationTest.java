package chevsavshev.com.cipher.integration;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import chevsavshev.com.cipher.service.model.encryptor.CipherProvider;
import chevsavshev.com.cipher.service.model.encryptor.DesEncryptor;
import chevsavshev.com.cipher.service.model.encryptor.SecretKeyGenerator;
import chevsavshev.com.cipher.util.ByteHexConverter;
import chevsavshev.com.cipher.util.Logger;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class DesEncryptorIntegrationTest {
    private static final String PLAIN_WORD = "lol";
    private static final String ENCRYPTED_WORD = "61057FFA437F9A52";
    private static final String CIPHER_NAME = "cipher";
    private static final String KEY = "297585752913D007";

    private static final String ALGORITHM_NAME = "DES";

    private DesEncryptor mDesEncryptor;

    @Mock
    Logger mLogger;

    ByteHexConverter mByteHexConverter;
    CipherProvider mCipherProvider;
    SecretKeyGenerator mSecretKeyGenerator;

    @Before
    public void onSetup() {
        mByteHexConverter = new ByteHexConverter();
        mCipherProvider = new CipherProvider(mByteHexConverter);
        mSecretKeyGenerator = new SecretKeyGenerator(mByteHexConverter, mLogger);
        mDesEncryptor = new DesEncryptor(mLogger, ALGORITHM_NAME, mCipherProvider, mSecretKeyGenerator);
    }

    @Test
    public void testDecryption() {
        String decipheredText = mDesEncryptor.decipherText(ENCRYPTED_WORD, KEY);
        assertThat(decipheredText, is(PLAIN_WORD));
    }
}
