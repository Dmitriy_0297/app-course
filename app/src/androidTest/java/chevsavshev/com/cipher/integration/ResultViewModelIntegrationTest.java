package chevsavshev.com.cipher.integration;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import chevsavshev.com.cipher.service.model.CipherModel;
import chevsavshev.com.cipher.service.repository.clipboard.ClipboardRepository;
import chevsavshev.com.cipher.service.repository.localstorage.FileRepository;
import chevsavshev.com.cipher.util.Logger;
import chevsavshev.com.cipher.viewmodel.ResultViewModel;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class ResultViewModelIntegrationTest {
    private static final String CIPHER_KEY = "lol";
    private static final String CIPHERED_TEXT = "kek";

    private final CipherModel VALID_CIPHER_MODEL = new CipherModel(CIPHERED_TEXT, CIPHER_KEY);
    private final CipherModel INVALID_CIPHER_MODEL = new CipherModel(null, null);

    private ResultViewModel mResultViewModel;

    @Mock
    Logger mLogger;

    @Mock
    FileRepository mFileRepository;

    @Mock
    ClipboardRepository mClipboardRepository;

    @Before
    public void onStartup() {
        MockitoAnnotations.initMocks(this);

        mResultViewModel = new ResultViewModel(mLogger, mFileRepository, mClipboardRepository);
    }

    @Test
    public void testCipherModelText() {
        mResultViewModel.setCipherResult(VALID_CIPHER_MODEL);

        assertThat(mResultViewModel.getResultText().get(), is(CIPHERED_TEXT));
    }

    @Test
    public void testCipherModelHasKey() {
        mResultViewModel.setCipherResult(VALID_CIPHER_MODEL);

        assertThat(mResultViewModel.getHasKey().get(), is(true));
    }

    @Test
    public void testCipherModelGetKey() {
        mResultViewModel.setCipherResult(VALID_CIPHER_MODEL);

        assertThat(mResultViewModel.getEncryptionKey().get(), is(CIPHER_KEY));
    }

    @Test
    public void testCipherModelWithoutKey() {
        mResultViewModel.setCipherResult(INVALID_CIPHER_MODEL);

        assertThat(mResultViewModel.getHasKey().get(), is(false));
    }
}
