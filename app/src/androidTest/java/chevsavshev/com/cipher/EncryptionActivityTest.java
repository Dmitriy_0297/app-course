package chevsavshev.com.cipher;

import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import chevsavshev.com.cipher.view.ui.EncryptionActivity;
import chevsavshev.com.cipher.view.ui.ResultActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static chevsavshev.com.cipher.common.Matchers.atPosition;

@RunWith(AndroidJUnit4.class)
public class EncryptionActivityTest {
    private static final String TEST_PLAIN_TEXT = "lol";
    private static final String TEST_CIPHER_NAME = "DES";

    @Rule
    public ActivityTestRule<EncryptionActivity> activityRule = new ActivityTestRule<>(
            EncryptionActivity.class, true, false);

    @Before
    public void setUp() {
        Intents.init();
        Intent intent = new Intent();
        intent.putExtra(Intent.EXTRA_TEXT, TEST_PLAIN_TEXT);

        activityRule.launchActivity(intent);
    }

    @Test
    public void testPlainTextIntent() {
        intended(hasExtra(Intent.EXTRA_TEXT, TEST_PLAIN_TEXT));
    }

//    ENCRYPTION_CIPHER_CHOICE_01
    @Test
    public void testHasCipher() {
        onView(withId(R.id.encryption_cipher_list))
                .check(matches(atPosition(0, hasDescendant(withText(TEST_CIPHER_NAME)))));
    }

    //    ENCRYPTION_CIPHER_CHOICE_02
    @Test
    public void testNextActivityTransition() {
        onView(withId(R.id.encryption_cipher_list))
                .perform(RecyclerViewActions.actionOnItem(
                        hasDescendant(withText(TEST_CIPHER_NAME)),
                        click()));

        intended(hasComponent(ResultActivity.class.getName()));
    }

    @After
    public void tearDown() {
        Intents.release();
    }
}
