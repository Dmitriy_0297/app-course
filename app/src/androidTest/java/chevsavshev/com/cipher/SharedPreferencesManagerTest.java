package chevsavshev.com.cipher;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import chevsavshev.com.cipher.util.SharedPreferencesManager;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class SharedPreferencesManagerTest {
    private SharedPreferencesManager mSharedPreferencesManager;

    private static final String TEST_KEY = "kek";
    private static final String TEST_TEXT = "lol";
    private static final String DEFAULT_VALUE = "cheburek";

    @Before
    public void onSetup() {
        mSharedPreferencesManager = new SharedPreferencesManager(InstrumentationRegistry.getTargetContext());
    }

    @Test
    public void testSaving() {
        mSharedPreferencesManager.storeString(TEST_KEY, TEST_TEXT);

       SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry.getTargetContext());
       String savedText = sharedPreferences.getString(TEST_KEY, null);

       assertThat(savedText, is(TEST_TEXT));
    }

    @Test
    public void testLoading() {
        SharedPreferences.Editor preferencesEditor = PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry.getTargetContext()).edit();
        preferencesEditor.putString(TEST_KEY, TEST_TEXT);
        preferencesEditor.commit();

        String loadedText = mSharedPreferencesManager.getString(TEST_KEY, null);

        assertThat(loadedText, is(TEST_TEXT));
    }

    @Test
    public void testDefaultValue() {
        String loadedText = mSharedPreferencesManager.getString(TEST_KEY, DEFAULT_VALUE);

        assertThat(loadedText, is(DEFAULT_VALUE));
    }

    @After
    public void onTearDown() {
        SharedPreferences.Editor preferencesEditor = PreferenceManager.getDefaultSharedPreferences(InstrumentationRegistry.getTargetContext()).edit();
        preferencesEditor.clear();
        preferencesEditor.commit();
    }
}
