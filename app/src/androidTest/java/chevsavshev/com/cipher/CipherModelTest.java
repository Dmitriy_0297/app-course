package chevsavshev.com.cipher;

import android.os.Parcel;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import chevsavshev.com.cipher.service.model.CipherModel;

import static junit.framework.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class CipherModelTest {

    @Test
    public void testParcelling() {
        String TEXT = "bla bla bla";
        String KEY = "tssss";

        CipherModel originalModel = new CipherModel(TEXT, KEY);

        Parcel parcel = Parcel.obtain();
        originalModel.writeToParcel(parcel, 0);

        parcel.setDataPosition(0);

        CipherModel ModelFromParcel = CipherModel.CREATOR.createFromParcel(parcel);
        assertEquals(originalModel.getText(), ModelFromParcel.getText());
        assertEquals(originalModel.getCipherKey(), ModelFromParcel.getCipherKey());
    }
}
